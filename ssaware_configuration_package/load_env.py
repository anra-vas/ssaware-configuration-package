import os
import socket

from dotenv import load_dotenv

from ssaware_configuration_package.constants import Constants

klemens_hosts: tuple = ('Klemens-MacBook-Pro.local', 'KlemenSpruks-MacBook-Pro.local',
                        'KlemenSpruksMBP.velis.office', 'Klemens-MBP.velis.office')

development_hosts: tuple = ()


def get_env_file() -> str:
    hs_name: str = socket.gethostname()
    if hs_name in klemens_hosts:
        return '/.env_klemens'
    elif hs_name in development_hosts:
        return '/.env_dev'
    return '/.env_prod'


def is_dev() -> bool:
    return socket.gethostname() in klemens_hosts


def load_env() -> None:
    load_dotenv(dotenv_path=os.path.dirname(os.path.realpath(__file__)) + get_env_file())
    os.environ['ANRA_OAUTH_PUBLIC_KEY'] = Constants.ANRA_OAUTH_PUBLIC_KEY
    os.environ['INTERNAL_APPS_TOKEN'] = Constants.INTERNAL_APPS_TOKEN
