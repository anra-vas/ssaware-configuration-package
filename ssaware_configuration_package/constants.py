import os
from typing import Optional


def get_oauth_pub_key() -> Optional[str]:
    with open(os.path.dirname(os.path.realpath(__file__)) + '/anra_oauth_public.key') as key_pointer:
        key: str = key_pointer.read()
    key_pointer.close()
    return key


class Constants(object):
    USA_ALPHA2_COUNTRY_CODE: str = 'US'
    WGS84_EPSG_CODE: int = 4326
    ANRA_OAUTH_PUBLIC_KEY = get_oauth_pub_key()

    INTERNAL_APPS_TOKEN = "84931a56-615e-4002-9809-12bce4044a32"
    INTERNAL_TOKEN_HEADER_NAME = "INTERNAL-TOKEN"

    class Formats(object):
        TIFF_FORMAT = 'tif'
        ARC_GRID_FORMAT = 'ArcGrid'
        SRTM_HGT_FORMAT = 'hgt'
        GEOJSON_FORMAT = 'geojson'
        SHAPEFILE_FORMAT = 'shp'
        AIP_FORMAT = 'aip'
